/*
 * urns.cpp
 *
 *  Created on: Aug 8, 2017
 *      Author: wegmannd
 */


#include "TUrns.h"

//---------------------------------------------------------------------------
//Main function
//---------------------------------------------------------------------------
int main(int argc, char* argv[]){
	struct timeval start, end;
    gettimeofday(&start, NULL);

	TLog logfile;
	logfile.newLine();
	logfile.write(" Urns 1.0 ");
	logfile.write("**********");
    try{
		//read parameters from the command line
    	TParameters myParameters(argc, argv, &logfile);

		//verbose?
		bool verbose = myParameters.parameterExists("verbose");
		if(!verbose) logfile.listNoFile("Running in silent mode (use 'verbose' to get a status report on screen)");
		logfile.setVerbose(verbose);
		//warnings?
		bool suppressWarnings=myParameters.parameterExists("suppressWarnings");
		if(suppressWarnings){
			logfile.list("Suppressing Warnings");
			logfile.suppressWarings();
		}


		//open log file that handles the output
		std::string  logFilename=myParameters.getParameterString("logFile", false);
		if(logFilename.length()>0){
			logfile.openFile(logFilename.c_str());
			logfile.writeFileOnly(" Urns 1.0 ");
			logfile.writeFileOnly("**********");
		}

		//creat core object
		TUrns urns(&logfile);

		//what to do?
		std::string task = myParameters.getParameterString("task");
		if(task=="simulate"){
			logfile.startIndent("Generating simulations (task = simulate):");
			urns.simulate(myParameters);
		} else if(task == "estimateEM"){
			logfile.startIndent("Estimating using an EM algorithm (task = estimateEM):");
			urns.estimateEM(myParameters);
		} else if(task == "estimateMCMC"){
			logfile.startIndent("Estimating using an MCMC algorithm (task = estimateMCMC):");
			urns.estimateMCMC(myParameters);
		} else throw "Unknown task '" + task + "'!";

		logfile.clearIndent();

		//write unsused parameters
		std::string unusedParams=myParameters.getListOfUnusedParameters();
		if(unusedParams!=""){
			logfile.newLine();
			logfile.warning("The following parameters were not used: " + unusedParams + "!");
		}
    }
	catch (std::string & error){
		logfile.error(error);
	}
	catch (const char* error){
		logfile.error(error);
	}
	catch(std::exception & error){
		logfile.error(error.what());
	}
	catch (...){
		logfile.error("unhandeled error!");
	}
	logfile.clearIndent();
	gettimeofday(&end, NULL);
	float runtime=(end.tv_sec  - start.tv_sec)/60.0;
	logfile.list("Program terminated in ", runtime, " min!");
	logfile.close();

    return 0;
}


