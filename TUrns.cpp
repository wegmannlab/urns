/*
 * TUrns.cpp
 *
 *  Created on: Aug 8, 2017
 *      Author: wegmannd
 */



#include "TUrns.h"


void TUrns::initializeRandomGenerator(TParameters & params){
	logfile->listFlush("Initializing random generator ...");
	if(params.parameterExists("fixedSeed")){
		randomGenerator=new TRandomGenerator(params.getParameterLong("fixedSeed"), true);
	} else if(params.parameterExists("addToSeed")){
		randomGenerator=new TRandomGenerator(params.getParameterLong("addToSeed"), false);
	} else randomGenerator=new TRandomGenerator();
	logfile->write(" done with seed " + toString(randomGenerator->usedSeed) + "!");
	randomGeneratorInitialized = true;
}

void TUrns::initializeUrns(TParameters & params){
	N = params.getParameterIntWithDefault("N", 9);
	if(N < 1) throw "N must be > 0!";
	nUrns = N + 1;

	//bionomial probabilities
	binomProb = new double[nUrns];
	for(int i=0; i<nUrns; ++i)
		binomProb[i] = (double) i / (double) N;

	urnsInitialized = true;

	logfile->list("Will use N = " + toString(N) + ": " + toString(nUrns) + " urns with 0 through " + toString(N) + " black balls.");
}

void TUrns::initialize(TParameters & params){
	initializeRandomGenerator(params);
	initializeUrns(params);
};
//------------------------------------------------------
//SIMULATE
//------------------------------------------------------
void TUrns::simulateOneExperiment(const int & m, double* piCumul, gz::ogzstream & out){
	int _urn = randomGenerator->pickOne(nUrns, piCumul);

	//sample m balls
	int _numBlack = randomGenerator->getBiomialRand(binomProb[_urn], m);

	//write
	out << m << "\t" << _numBlack << "\t" << _urn << "\n";
}

void TUrns::simulate(TParameters & params){
	//read simulation parameters
	logfile->startIndent("Reading simulation parameters:");
	initialize(params);

	//number of samples / experiments
	long numSamples = params.getParameterIntWithDefault("samples", 1000);
	logfile->list("Will simulate " + toString(numSamples) + " samples / experiments.");
	if(numSamples < 1) throw "m must be > 0!";

	//number of balls per draw
	bool fixedM;
	int m;
	double mMean, mSD;
	if(params.parameterExists("mMean")){
		fixedM = false;
		mMean = params.getParameterDouble("mMean");
		if(mMean <= 0.0) throw "mMean must be >= 0.01!";
		if(!params.parameterExists("mSD"))
			throw "mSD must be defined if mMean is given!";
		mSD = params.getParameterDouble("mSD");
		if(mSD <= 0.0) throw "mMean must be > 0.0!";
		logfile->list("Will sample number of balls to draw m ~ N(" + toString(mMean) + ", " + toString(mSD), ").");
	} else {
		fixedM = true;
		m = params.getParameterIntWithDefault("m", 20);
		logfile->list("Will draw m = " + toString(m) + " balls with replacement per experiment.");
		if(m < 1) throw "m must be > 0!";
	}

	//read pis
	TPi pi(nUrns, params, logfile, randomGenerator);

	//fill cumulative and binom prob
	pi.fillcumulative();

	//output name
	std::string outname = params.getParameterStringWithDefault("out", "urn_simulations");
	logfile->list("Will write output files with prefix '" + outname + "'.");

	//num replicates
	int numReps = params.getParameterIntWithDefault("reps", 1);
	logfile->list("Will simulate " + toString(numReps) + " replicates.");
	logfile->endIndent();

	//now simulate ...
	//----------------
	//prepare variables
	long i;
	std::string filename;
	gz::ogzstream out;

	//Loop over replicates
	logfile->startIndent("Performing simulations:");
	for(int r=0; r<numReps; ++r){
		logfile->listFlush("Generating replicate " + toString(r+1) + " ...");
		//open output file
		filename = outname + "_rep" + toString(r+1) + ".txt.gz";
		out.open(filename.c_str());
		if(!out) throw "Failed to open file '" + filename + "' for writing!";

		//writer header
		out << "m\tnBlack\ttrueUrn\n";


		//loop
		if(fixedM){
			for(i=0; i<numSamples; ++i)
				simulateOneExperiment(m, pi.pointerToCumulative(), out);
		} else {
			for(i=0; i<numSamples; ++i){
				//sample m, exclude m=0
				m = round(randomGenerator->getNormalRandom(mMean, mSD));
				if(m > 0)
					simulateOneExperiment(m, pi.pointerToCumulative(), out);
				else
					i--;
			}
		}

		//close output file
		out.close();
		logfile->done();
	}
}

//------------------------------------------------------
//Estimate using EM
//------------------------------------------------------
void TUrns::fillBinomialDensities(int _maxM){
	if(binomDensInitialized){
		if(_maxM != maxM)
			freeBinomialDensities();
	}
	maxM = _maxM;

	//pre-calculate all binomial densities
	binomDens = new double**[nUrns];
	int u,m,i;
	for(u=0; u<nUrns; ++u){
		binomDens[u] = new double*[maxM+1];
		for(m=1; m<=maxM; ++m){
			binomDens[u][m] = new double[m+1];

			//now fill binomial densities
			for(i=0; i<m+1; ++i){
				binomDens[u][m][i] = randomGenerator->binomDensity(m, i, binomProb[u]);
			}
		}
	}
	binomDensInitialized = true;
}

void TUrns::freeBinomialDensities(){
	if(binomDensInitialized){
		for(int u=0; u<nUrns; ++u){
			for(int m=1; m<=maxM; ++m)
				delete[] binomDens[u][m];
			delete[] binomDens[u];
		}
		delete[] binomDens;
		binomDensInitialized = false;
	}
}

void TUrns::readData(std::string filename){
	logfile->listFlush("Reading data from file '" + filename + "' ...");
	gz::igzstream in(filename.c_str());
	if(!in) throw "Failed to open file '" + filename + "' for reading!";

	//skip header
	std::string line;
	getline(in, line);

	//now parse file
	long lineNum = 1;
	std::vector<int> vec;
	int _maxM = 0;
	while(in.good() && !in.eof()){
		++lineNum;
		getline(in, line);
		trimString(line);
		if(!line.empty()){
			fillVectorFromStringWhiteSpaceSkipEmptyCheck(line, vec);
			if(vec.size() < 2)
				throw "Expect at least two columns (m and n) in file '" + filename + "' on line " + toString(lineNum) + "!";

			if(vec[0] > 0){
				data.push_back(std::pair<int,int>(vec[0], vec[1]));

				//find max m
				if(_maxM < vec[0])
					_maxM = vec[0];
			}
		}
	}

	//clean up
	in.close();
	logfile->done();
	logfile->conclude("Read " + toString(data.size()) + " data points");

	logfile->listFlush("Initializing binomial densities up to m = " + toString(_maxM) + " ...");
	fillBinomialDensities(_maxM);
	logfile->done();
}

double TUrns::getLL(TPi & _pi){
	double LL = 0.0;
	int u;
	double tmp;
	for(dataIt = data.begin(); dataIt != data.end(); ++dataIt){
		tmp = 0.0;
		for(u=0; u<nUrns; ++u){
			tmp += binomDens[u][dataIt->first][dataIt->second] * _pi[u];
		}
		LL += log(tmp);
	}
	return LL;
};

void TUrns::estimateEM(TParameters & params){
	logfile->startIndent("Reading parameters:");
	initialize(params);

	//initialize pi
	TPi pi(nUrns, params, logfile, randomGenerator);

	//EM params
	int numIter = params.getParameterIntWithDefault("nIter", 1000);
	logfile->list("Will run EM up to " + toString(numIter) + " iterations.");
	double tolerance = params.getParameterDoubleWithDefault("tolerance", 0.0);
	if(tolerance > 0.0)
		logfile->list("Will stop EM if LL improvement is < " + toString(tolerance) + ".");
	logfile->endIndent();

	//read data
	logfile->startIndent("Prepare data:");
	std::string dataFilename = params.getParameterString("data");
	readData(dataFilename);

	//get first LL
	double LL = getLL(pi);
	logfile->list("Initial LL = " + toString(LL));
	logfile->endIndent();

	//variables
	double* binomXpi = new double[nUrns];

	//now run EM
	logfile->startIndent("Running EM algorithm:");
	for(int i=0; i<numIter; ++i){
		logfile->listFlush("Running EM iteration " + toString(i+1) + " ...");

		//save old values
		double LL_old = LL;
		LL = 0.0;
		pi.saveOld();

		//now get new estimates
		for(dataIt = data.begin(); dataIt != data.end(); ++dataIt){
			double sum = 0.0;
			for(int u=0; u<nUrns; ++u){
				binomXpi[u] = binomDens[u][dataIt->first][dataIt->second] * pi.old(u);
				sum += binomXpi[u];
			}

			for(int u=0; u<nUrns; ++u){
				pi[u] += binomXpi[u] / sum;
			}
		}

		//normalize pi
		pi.normalize();

		logfile->done();

		if(tolerance > 0.0){
			//calc new LL
			LL = getLL(pi);

			//report
			logfile->conclude("LL = " + toString(LL));
			double diff = LL - LL_old;
			logfile->conclude("LL improved by " + toString(diff));

			//check if we abort
			if(LL - LL_old < tolerance){
				logfile->conclude("LL improvement < " + toString(tolerance) + ", aborting EM.");
				break;
			}
		}
	}
	logfile->endIndent();

	//report estimates
	logfile->list("Estimated pi are:" + pi.getStringOfPi());
	LL = getLL(pi);
	logfile->list("Final LL = " + toString(LL));

	//write estimates to file
	std::string filename = dataFilename;
	filename = extractBefore(filename, '.');
	filename += "_estimates.txt";
	std::ofstream res(filename.c_str());
	if(!res) throw "Failed to open file '" + filename + "' for writing!";
	res << pi[0];
	for(int u=1; u<nUrns; ++u)
		res << "\t" << pi[u];
	res << "\n";
	res.close();

	//clean up memory
	delete[] binomXpi;
};

void TUrns::estimateMCMC(TParameters & params){
	logfile->startIndent("Reading parameters:");
	initialize(params);

	//initialize pi
	TPi pi(nUrns, params, logfile, randomGenerator);

	//MCMC params
	int numIter = params.getParameterIntWithDefault("nIter", 10000);
	logfile->list("Will run MCMC up to " + toString(numIter) + " iterations.");
	double proposeSd = params.getParameterDoubleWithDefault("propSd", 0.1);
	logfile->list("Will propose new parameters as pi' = pi * exp(x), x ~ N(0, " + toString(proposeSd) + ").");
	logfile->endIndent();

	//read data
	logfile->startIndent("Prepare data:");
	std::string dataFilename = params.getParameterString("data");
	readData(dataFilename);

	//get first LL
	double LL = getLL(pi);
	logfile->list("Initial LL = " + toString(LL));
	logfile->endIndent();

	//open MCMC output file
	std::string filename = dataFilename;
	filename = extractBefore(filename, '.');
	filename += "_MCMC.txt";
	TOutputFileZipped out(filename);

	//write header and initial values
	std::vector<std::string> header = {"LL"};
	for(int u=0; u<nUrns; ++u)
		header.push_back("pi_" + toString(u));
	out.writeHeader(header);
	out << LL;
	pi.write(&out);

	//run MCMC
	std::string reportString = "Running an MCMC for " + toString(numIter) + " iterations ... ";
	int prog = 0; int oldProg = 0;
	logfile->listFlush(reportString + "(" + toString(prog) + "%)");
	int numAccepted = 0;

	for(int i=0; i<numIter; ++i){
		//propose new pi
		pi.proposeMCMC(randomGenerator, proposeSd);

		//accept or reject
		double oldLL = LL;
		LL = getLL(pi);
		double logH = LL - oldLL;

		double logp = log(randomGenerator->getRand());
		if(logp < logH){
			++numAccepted;
		} else {
			//reject
			pi.rejectCurrent();
			LL = oldLL;
		}

		//write LL and new pi
		out << LL;
		pi.write(&out);
		pi.addToMeanVar();

		//report progress
		prog = 100 * (double) i / (double) numIter;
		if(prog > oldProg){
			logfile->listOverFlush(reportString + "(" + toString(prog) + "%)");
			oldProg = prog;
		}
	}
	logfile->overList(reportString + "done!    ");
	logfile->conclude("Acceptance rate was " + toString((double) numAccepted / (double) numIter));

	//write mean / variance
	filename = dataFilename;
	filename = extractBefore(filename, '.');
	filename += "_posterior.txt";
	pi.writeMeanVar(filename);
};




