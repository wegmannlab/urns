/*
 * TPi.h
 *
 *  Created on: Mar 24, 2019
 *      Author: phaentu
 */

#ifndef TPI_H_
#define TPI_H_

#include "TRandomGenerator.h"
#include "TParameters.h"
#include "TFile.h"
#include "TMeanVar.h"

class TPi{
private:
	int nUrns;
	double* pi;
	double* oldPi;
	double* cumulPi;
	bool cumulativeInitialized;
	TMeanVar* meanVar;
	bool meanVarInitialized;

	void readPi(TParameters & params, TLog* logfile){
		std::vector<std::string> tmp;
		std::vector<double> tmpDouble;
		params.fillParameterIntoVector("pi", tmp, ',');

		repeatIndexes(tmp, tmpDouble);
		if(tmpDouble.size() != (unsigned int) nUrns)
			throw "Number of pi provided (" + toString(tmpDouble.size()) + ") does not match number of urns (" + toString(nUrns) + ")!";

		//normalize
		double sum = 0.0;
		for(std::vector<double>::iterator it=tmpDouble.begin(); it!=tmpDouble.end(); ++it)
			sum += *it;
		if(sum != 1.0)
			logfile->warning("Pi do not sum to 1.0 but " + toString(sum) + "! Will normalize ...");
		int i=0;
		for(std::vector<double>::iterator it=tmpDouble.begin(); it!=tmpDouble.end(); ++it, ++i)
			pi[i] = *it / sum;
	};

	void initPisFromTheta(double theta){
		//initialize pis from theta (like neutral SFS)
		double sum = 0.0;
		for(int i=1; i<nUrns; ++i){
			pi[i] = theta / (double) i;
			sum += pi[i];
		}
		if(sum > 1.0)
			throw "Provided theta results in sum(pi) > 1.0 (sum(pi) = " + toString(sum) + "!";

		pi[0] = 1.0 - sum;
	};

	void initRandomPi(TRandomGenerator* randomGenerator){
		for(int i=0; i<nUrns; ++i)
			pi[i] = randomGenerator->getRand();

		normalize();
	}

	void initUniformPi(){
		for(int i=0; i<nUrns; ++i){
			pi[i] = 1.0 / (double) nUrns;
		}
	};

public:
	TPi(int NumUrns, TParameters & params, TLog* logfile, TRandomGenerator* randomGenerator){
		//allocate memory
		nUrns = NumUrns;
		pi = new double[nUrns];
		oldPi = new double[nUrns];
		cumulativeInitialized = false;
		meanVarInitialized = false;

		if(params.parameterExists("pi")){
			readPi(params, logfile);
		} else if(params.parameterExists("theta")){
			//initialize pis from theta (like neutral SFS)
			double theta = params.getParameterDouble("theta");
			logfile->list("Will initialize pi from theta = " + toString(theta) + ".");
			initPisFromTheta(theta);
		} else if(params.parameterExists("randomPi")){
			logfile->list("Will initialize pi randomly.");
			initRandomPi(randomGenerator);
		} else {
			logfile->list("Will initialize uniform pi.");
			initUniformPi();
		}

		//print pis used
		logfile->conclude("Will use these pi: " + getStringOfPi());
	};

	~TPi(){
		delete[] pi;
		delete[] oldPi;
		if(cumulativeInitialized)
			delete[] cumulPi;
	};

	double& operator[](int i){ return pi[i]; };
	double& old(int i){ return oldPi[i]; };
	double& cumul(int i){ return cumulPi[i]; };
	double* pointerToCumulative(){ return cumulPi; };

	void normalize(){
		double sum = 0.0;
		for(int u=0; u<nUrns; ++u)
			sum +=	pi[u];
		for(int u=0; u<nUrns; ++u)
			pi[u] /= sum;
	};

	void setZero(){
		for(int u=0; u<nUrns; ++u)
			pi[u] = 0.0;
	};

	void fillcumulative(){
		//initialize
		if(!cumulativeInitialized){
			cumulPi = new double[nUrns];
			cumulativeInitialized = true;
		}

		//now fill
		cumulPi[0] = pi[0];
		for(int i=1; i<nUrns; ++i){
			cumulPi[i] = cumulPi[i-1] + pi[i];
		}

	};

	std::string getStringOfPi(){
		std::string piString = toString(pi[0]);
		for(int i=1; i<nUrns; ++i){
			piString += ", " + toString(pi[i]);
		}
		return piString;
	};

	void saveOld(){
		double* tmp = oldPi;
		oldPi = pi;
		pi = tmp;
		setZero();
	};

	void rejectCurrent(){
		double* tmp = oldPi;
		oldPi = pi;
		pi = tmp;
	};

	void proposeMCMC(TRandomGenerator* randomGenerator, double & sd){
		saveOld();

		//pick random pi to update
		int u = randomGenerator->pickOne(nUrns);

		//choose by how much to change
		pi[u] = oldPi[u] * exp(randomGenerator->getNormalRandom(0.0, sd));

		//scale others
		double diff = (pi[u] - oldPi[u]) / (nUrns - 1.0);
		for(int i=0; i<nUrns; ++i){
			if(i != u) pi[i] = oldPi[i] - diff;
		}
	};

	void write(TOutputFile* out){
		out->write(pi[0]);
		for(int u=1; u<nUrns; ++u)
			out->write(pi[u]);
		out->endLine();
	};

	void addToMeanVar(){
		if(!meanVarInitialized){
			meanVar = new TMeanVar[nUrns];
			meanVarInitialized = true;
		}

		for(int i=0; i<nUrns; ++i)
			meanVar[i].add(pi[i]);
	};

	void writeMeanVar(std::string filename){
		if(!meanVarInitialized)
			throw "Mean and variance never initialized!";

		TOutputFilePlain out(filename);
		out.writeHeader({"urn", "mean_pi", "var_pi"});

		for(int i=0; i<nUrns; ++i){
			meanVar[i].calcMeanVar();
			out << i << meanVar[i].mean << meanVar[i].var << std::endl;
		}
	};
};


#endif /* TPI_H_ */
