/*
 * TUrns.h
 *
 *  Created on: Aug 8, 2017
 *      Author: wegmannd
 */

#ifndef TURNS_H_
#define TURNS_H_

#include "TPi.h"
#include <zlib.h>
#include "gzstream.h"

class TUrns{
private:
	TLog* logfile;
	TRandomGenerator* randomGenerator;
	bool randomGeneratorInitialized;

	int N;
	int nUrns;
	double* binomProb;
	bool urnsInitialized;

	int maxM;
	double*** binomDens;
	bool binomDensInitialized;

	std::vector< std::pair<int,int> > data; //pair of m and num black balls
	std::vector< std::pair<int,int> >::iterator dataIt;

	void initializeRandomGenerator(TParameters & params);
	void initializeUrns(TParameters & params);
	void initialize(TParameters & params);

	void simulateOneExperiment(const int & m, double* piCumul, gz::ogzstream & out);
	void fillBinomialDensities(int _maxM);
	void readData(std::string filename);
	void freeBinomialDensities();
	double getLL(TPi & _pi);

public:
	TUrns(TLog* Logfile){
		logfile = Logfile;
		randomGenerator = NULL;
		randomGeneratorInitialized = false;

		N = 0;
		nUrns = 0;
		binomProb = NULL;
		urnsInitialized = false;

		maxM = 0;
		binomDens = NULL;
		binomDensInitialized = false;
	};
	~TUrns(){
		if(randomGeneratorInitialized)
			delete randomGenerator;
		if(urnsInitialized)
			delete[] binomProb;
		freeBinomialDensities();
	};

	void simulate(TParameters & params);
	void estimateEM(TParameters & params);
	void estimateMCMC(TParameters & params);
};


#endif /* TURNS_H_ */
